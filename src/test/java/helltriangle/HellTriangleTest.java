package helltriangle;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

import br.com.b2w.helltriangle.HellTriangle;

public class HellTriangleTest{

	@Test
	public void calculateMaxTotalWith4Lines() {
		int[][] input1 = new int[][] {{6},{3,5},{9,7,1},{4,6,8,4}};
		assertEquals("Tem que ser 26", 26, HellTriangle.calculateMaxTotal(input1));
		int[][] input2 = new int[][] {{7},{4,6},{8,6,4},{6,3,2,1}};
		assertEquals("Tem que ser 25", 25, HellTriangle.calculateMaxTotal(input2));
	}
	
	@Test
	public void calculateMaxTotalWith5Lines() {
		int[][] input1 = new int[][] {{6},{3,5},{9,7,1},{4,6,8,4},{4,5,8,3,2}};
		assertEquals("Tem que ser 34", 34, HellTriangle.calculateMaxTotal(input1));
		int[][] input2 = new int[][] {{7},{4,6},{8,6,4},{6,3,2,1},{6,6,8,2,1}};
		assertEquals("Tem que ser 31", 31, HellTriangle.calculateMaxTotal(input2));
	}


}
