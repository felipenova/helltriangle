package br.com.b2w.helltriangle;

/**
 * Classe usada para fazer o calculo da maior soma de numeros de um conjunto de arrays que formam
 * um triangulo
 * @author Felipe Calaca Pita Pombo da Nova
 *
 */
public final class HellTriangle {
	
	/**
	 * Calculando usando o caminho de "bottom up", ou seja partindo da base da piramide 
	 * para o topo, comecando o calculo da penultima linha da piramide pegando o valor e 
	 * somando com o maximo valor dos dois numeros abaixo dele e assim por diante, ate que
	 * se chegue ao topo.
	 * @param input
	 * @return
	 */
	public static int calculateMaxTotal(int[][] input){
        for (int x = input.length - 1; x > 0; x--){
            for (int y = 0; y < input[x].length - 1; y++){
            	input[x - 1][y] += Math.max(input[x][y], input[x][y + 1]);
            }
        }
        return input[0][0];
	}

}
